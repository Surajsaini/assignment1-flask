from flask import Flask, render_template, request,redirect, url_for, session,flash, jsonify
from functools import wraps
from flask_sqlalchemy import SQLAlchemy
from flask import Flask, Response
import json
import os
from datetime import datetime
from enum import Enum
from uuid import UUID


app = Flask(__name__)

app.config['SECRET_KEY'] = '9OLWxND4o83j4K4iuopO'
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:1234@127.0.0.1/Users'
db = SQLAlchemy(app)

primitive = (int, float, str, bool)

def is_primitive(thing):
    return isinstance(thing, primitive)

def serialize(obj, datetime_format=None):
    if obj is None:
        return None
    if is_primitive(obj):
        return obj
    if isinstance(obj, Enum):
        return obj.value
    if isinstance(obj, datetime):
        return obj.isoformat() if not datetime_format else obj.strftime(datetime_format)
    if isinstance(obj, UUID):
        return str(obj)
    if isinstance(obj, list):
        return [serialize(v, datetime_format=datetime_format) for v in obj]
    if isinstance(obj, dict):
        return {
    k: serialize(v, datetime_format=datetime_format) for k, v in obj.items()
    }
    if isinstance(obj, db.Model):
        return {
        column.key: serialize(getattr(obj, attr), datetime_format=datetime_format)
        for attr, column in obj.__mapper__.c.items()
        }
    # if isinstance(obj, WKBElement):
    #     return {}
    return {
        k: serialize(v, datetime_format=datetime_format)
        for k, v in obj.__dict__.items()
    }

class User(db.Model):

    __tablename__ = "users"

    id = db.Column(db.Integer,autoincrement=True, primary_key=True)
    createdAt = db.Column(db.DateTime, server_default=db.func.now())
    updatedAt = db.Column(db.DateTime, server_default=db.func.now(), server_onupdate=db.func.now())
    email = db.Column(db.String(30), unique=True)
    mobile = db.Column(db.String(30), unique=True)
    address = db.Column(db.String(50), unique=True)
    password = db.Column(db.String(20))
    name = db.Column(db.String(50))


    def save(self):
        db.session.add(self)
        db.session.commit()

class Contact(db.Model):

    __tablename__ = "contacts"

    id = db.Column(db.Integer,autoincrement=True, primary_key=True)
    createdAt = db.Column(db.DateTime, server_default=db.func.now())
    updatedAt = db.Column(db.DateTime, server_default=db.func.now(), server_onupdate=db.func.now())
    email = db.Column(db.String(30), unique=True, nullable=False)
    mobile = db.Column(db.String(20), nullable=False)
    name = db.Column(db.String(50), nullable=False)
    address = db.Column(db.String(50), nullable=False)
    country = db.Column(db.String(50), nullable=False)


    def save(self):
        db.session.add(self)
        db.session.commit()

# class ContactSchema(ma.Schema):
#     class Meta:
#         fields = ('id','name','mobile','email','address','country')

# contact_schema = ContactSchema(strict=True)
# contacts_schema = ContactSchema(strict=True)

def login_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'logged_in' in session:
            return f(*args, **kwargs)
        else:
            flash('You need to login first')
            return redirect(url_for('login'))
    return wrap

@app.route("/api", methods=['GET'])
def get():
   return jsonify({ 'msg':'Hello World'})

@app.route("/", methods=['GET', 'POST'])
def home():
   return render_template('index.html')

@app.route('/welcome')
def welcome():
    return render_template('welcome.html')

@app.route('/register', methods=['GET','POST'])
def register():
    if request.method == 'POST':
        name = request.form.get('name')
        email = request.form.get('email')
        mobile = request.form.get('mobile')
        address = request.form.get('address')
        password = request.form.get('password')

        user = User(name=name,email=email,mobile=mobile,address=address,password=password)
        user.save()
        flash("You were registered successfully!")
        return redirect(url_for('login'))
    
    return render_template('register.html')
    

@app.route('/login', methods = ['GET', 'POST'])

def login():
    error=None
    if request.method == 'POST':
        name = request.form.get('name')
        password = request.form.get('password')
        user = User.query.filter_by(name=name, password=password).first()
        if user is not None:
            session['logged_in'] = True
            flash("You were just logged in!")
            return redirect(url_for('home'))
        else:
           flash("Invalid username or password.")
    return render_template('login.html', error=error)

@app.route('/logout')
@login_required
def logout():
    session.pop('logged_in', None)
    flash("You were just logged out!")
    return redirect(url_for('welcome'))

@app.route('/users', methods=['GET'])
@login_required
def getUsers():
    users = User.query.all()
    print(users)
    # return Response(json.dumps({'data': serialize(users)}), mimetype="application/json")
    return render_template("users.html", users=users)

@app.route('/contacts/details', methods = ['GET'])
# @login_required
def showContact():
   if request.method == 'GET':
       contacts = Contact.query.all()
       print(contacts)
       return Response(json.dumps({'data': serialize(contacts)}), mimetype="application/json")
       return response

    #    return render_template("contactDetail.html", contacts=contacts)

@app.route('/contacts', methods = ['GET'])
# @login_required
def showContacts():
   if request.method == 'GET':
       contacts = Contact.query.all()
    #    result = contacts_schema.dump(contacts)
       return render_template("contactDetail.html", contacts=contacts)

@app.route('/contacts/new/', methods=['GET', 'POST'])
@login_required
def newContact():
    if request.method == 'POST':
        name = request.form.get('name')
        email = request.form.get('email')
        mobile = request.form.get('mobile')
        address = request.form.get('address')
        country = request.form.get('country')
        contacts = Contact(name=name,email=email,mobile=mobile,address=address,country=country)
        contacts.save()
        return redirect(url_for('showContacts'))
    else:
        return render_template('addContact.html')

if __name__ == '__main__':
    db.create_all()
    app.run(debug=True)
    
    